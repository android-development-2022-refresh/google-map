---
title: Set up a map
template: main-repo.html
---

Let's take our new key and add it to the project.

First, add the name of the key to your `local.properties` file in the root directory of your project. This file should **NEVER** be checked into git, as the key name is a secret. (Because this file isn't checked in, and I'm using git to generate the code display on the right, I'll show its contents below)

```
## This file is automatically generated by Android Studio.
# Do not modify this file -- YOUR CHANGES WILL BE ERASED!
#
# This file should *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.
sdk.dir=D\:\\Android\\sdk
MAPS_API_KEY=YOUR_KEY_NAME_GOES_HERE
```

(Don't worry about that comment at the top; the MAPS_API_KEY really goes here. If you re-checkout the project, you'll need to add this key again)

To access the key, we'll use the "secrets" plugin, which reads `local.properties` and makes the properties available in other files.

Declare the secrets plugin {{ find("030-add-secrets-plugin", "in the root build.gradle") }}, and apply it in the {{ find("030-use-secrets-plugin", "app module build.gradle") }}.

Then we can {{ find("030-manifest-key", "drop it in the Manifest") }} for Google Maps to find at runtime.

So far we don't have access to the Google Maps @Composable function, so we {{ find("030-add-map-deps", "add the map dependencies") }}.

Finally! Time to {{ find("030-add-map", "Add the Map to the UI") }}!

When we run the app, we'll see

![We have a Map!](screenshots/map1.png){ width=400 }
