---
title: Project Setup
template: main-repo.html
---

We start with the "Empty Compose Activity" project wizard.

Update {{ find("020-compose-and-kotlin-versions", "Compose and Kotlin Versions") }} and the
{{ find("020-latest-android-sdk", "Latest Android SDK") }}.

Be sure to change the {{ find("020-use-variable-from-main-build", "kotlinCompilerExtensionVersion") }} to use the variable from the main build.gradle.

Finally, look at the {{ find("020-update-all-highlighted-deps", "app module dependencies") }}. Press alt-enter on all that are highlighted as out of date to update them. (Note that the ones using the `$compose_ui_version` reference should stop being a warning after syncing)

Sync the gradle files with Android Studio and all the warnings should disappear.
