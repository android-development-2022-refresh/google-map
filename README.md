# Google Map

NOTE - to build this application, you need to 

   * create an API key in the Google Developers Console
   * Copy the key into the local.properties file 
     
     ```
     MAPS_API_KEY=COPY_KEY_HERE
     ```

   * Update the key to only allow the application

(Alternatively you can comment out the &lt;meta-data%gt; block in the AndroidManifest.xml until after you've got your fingerprint)


Module Content: [https://androidbyexample.com/week-09/9.1-Google-map](https://androidbyexample.com/week-09/9.1-Google-map)
